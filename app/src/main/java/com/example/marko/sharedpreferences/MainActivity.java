package com.example.marko.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText nameField;
    EditText contactField;
    Button saveButton;
    Button  displayButton;
    TextView infoText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         nameField = (EditText) findViewById(R.id.name_text);
         contactField = (EditText) findViewById(R.id.contact_text);
         saveButton  = (Button) findViewById(R.id.button_save);
         displayButton  = (Button) findViewById(R.id.button_display);
         infoText = (TextView)  findViewById(R.id.infoText);
    }
        // Save the details
        public void saveInfo(View view){

            SharedPreferences myPreferences = getSharedPreferences("userdata", Context.MODE_PRIVATE);
            SharedPreferences.Editor myEditor = myPreferences.edit();
            myEditor.putString("username", nameField.getText().toString());
            myEditor.putString("contact", contactField.getText().toString());
            myEditor.apply();

            Toast.makeText(this, "Saved!", Toast.LENGTH_LONG).show();
        }

        //Print the saved data

            public void displayData(View view){

            SharedPreferences myPreferences = getSharedPreferences("userdata", Context.MODE_PRIVATE);
            String name = myPreferences.getString("username","");
            String number = myPreferences.getString("contact","");

            infoText.setText(name + " " + number);

            }
}
